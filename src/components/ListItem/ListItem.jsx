const ListItem = ({ label }) => {
    return <li className="list-item">{label}</li>;
};

export default ListItem;
