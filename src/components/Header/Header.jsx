import { Component } from 'react';
import ListItem from '../ListItem/ListItem';
import Sacolinha from '../../assets/icons/Sacolinha';
class Header extends Component {
    constructor(props) {
        super(props);
        this.state = { menus: [] };
    }

    componentDidMount() {
        console.log('componentDidMount');
        this.setState({
            menus: [
                {
                    label: 'Products'
                },
                {
                    label: 'Shopping Cart'
                },
                {
                    label: 'Sing in'
                }
            ]
        });
    }

    componentDidUpdate() {
        console.log('componentDidUpdate');
    }

    componentWillUnmount() {
        console.log('componentWillUnmount');
    }

    render() {
        const { menus } = this.state;
        return (
            <div>
                <header>
                    <div>
                        <Sacolinha />
                        <h2>Lojinha</h2>
                    </div>
                    <nav>
                        <ul>
                            {menus.map(({ label }) => (
                                <ListItem label={label} />
                            ))}
                        </ul>
                    </nav>
                </header>
            </div>
        );
    }
}

export default Header;
