const ROUTES_CONSTANTS = {
    HOME: '/',
    CATEGORIES: '/categories',
    CATEGORY: '/categories/:token',
    CART: '/cart',
    SIGN_IN: '/signin',
    SIGN_UP: '/signup'
};

export default ROUTES_CONSTANTS;
